<%@ taglib prefix="form" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
<c:out value="${errorString}"/>
<input type="button" onclick="location.href='/registration'" value="OK">
</body>
</html>
