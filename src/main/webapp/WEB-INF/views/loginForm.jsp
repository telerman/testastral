<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<html>
<head></head>
<body>
<h1>Login</h1>
<form name='f' action="login" method='POST'>
    <table>
        <tr>
            <td>User:</td>
            <td><input name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password' /></td>
        </tr>
        <tr>
            <td><input name="submit" type="submit" value="Login" /></td>
            <td><input type="button" onclick="location.href='/registration'" value="Register new" /></td>
        </tr>
    </table>
</form>
</body>
</html>