<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<form:form method="post" modelAttribute="user" action="/registration">
    <table>
        <tr>
            <td>Login:</td>
            <td><form:input path="login"/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><form:password path="password"/></td>
        </tr>
        <tr>
            <td>Repeat password</td>
            <td><form:password path="matchingPassword"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <form:button>Save</form:button>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>