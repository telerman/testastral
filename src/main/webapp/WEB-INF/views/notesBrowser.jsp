<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="windowName" value="Notes browser"/>

<html>
<head>
    <title>${windowName}</title>
</head>
<body>
<div style="width: 100%">
    <h1 align="left">${windowName}</h1>
    <a style="align-self: right" href="<c:url value="/logout" />">Logout</a>
</div>

<table border="1" cellspacing="2" width="70%">
    <form:form method="post" action="/">
        <tr>
            <td width="80%">
                <input name="searchString" style="width: 100%" value="${searchString}"/>
            </td>
            <td>
                <input type="submit" style="width: 100%" value="Search">
            </td>
            <td>
                <input type="button" style="width: 100%" value="Clear" onclick="location.href='/'">
            </td>
        </tr>
    </form:form>
</table>
<table border="2" width="70%" cellpadding="2">
    <tr>
        <td colspan="3">
            <input type="button" style="width: 30%" onclick="location.href='/createNote'" value="Create">
        </td>
    </tr>
    <tr>
        <th></th>
        <th>Title</th>
        <th>Created</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="note" items="${notesList}">
        <tr>
            <td width="10%" align="center" style="align-content: center">
                <svg width="90" height="90">
                    <image xlink: xlink:href="data:image/svg+xml;base64,${note.icon}" width="90" height="90"/>
                </svg>
            </td>
            <td width="80%" align="center"><a href="editNote/${note.id}">${note.header}</a></td>
            <td width="80%" align="center"><fmt:formatDate value="${note.createTs}" pattern="dd-MM-yyyy HH:mm" /></td>
            <td width="10%" align="center"><a href="deleteNote/${note.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>