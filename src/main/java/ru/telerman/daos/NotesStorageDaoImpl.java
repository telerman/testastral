package ru.telerman.daos;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import ru.telerman.model.Note;
import ru.telerman.utils.jdbc.NoteMapper;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Repository
public class NotesStorageDaoImpl implements NotesStorageDao {
    @Inject
    private JdbcTemplate jdbcTemplate;

    public void add(Note note) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String sql = "insert into notes(header, body, icon, create_ts, create_by) values (?,?,?,?,?)";
        jdbcTemplate.update(sql, note.getHeader(), note.getBody(), note.getIcon(),
                new Timestamp(new Date().getTime()),
                authentication.getName());
    }

    public List<Note> getList() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String sql = "select * from notes where create_by = ? ORDER BY create_ts desc";
        List<Note> list = jdbcTemplate.query(sql, new NoteMapper(), authentication.getName());
        return list;
    }

    public List<Note> getList(String searchString){
        if(searchString == null || searchString.isEmpty())
            return getList();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String sql = "select * from notes n where (upper(n.header) like ? OR upper(n.body) LIKE ?) and create_by = ? ORDER BY create_ts desc";
        String stParam = '%' + searchString.toUpperCase() + '%';
        List<Note> notes = jdbcTemplate.query(sql, new NoteMapper(), stParam, stParam, authentication.getName());
        return notes;
    }

    public Note get(long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String sql = "SELECT * FROM notes n where n.id = ? and n.create_by = ?";
        List<Note> query = jdbcTemplate.query(sql, new NoteMapper(), id, authentication.getName());
        return query.size() == 0 ? null : query.get(0);
    }

    public void update(Note note){
        final String sql = "UPDATE notes n set n.header = ?, n.body = ? WHERE n.id = ?";
        int update = jdbcTemplate.update(sql, note.getHeader(), note.getBody(), note.getId());
        if(update == 0)
            add(note);
    }

    public void delete(long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String sql = "DELETE FROM notes WHERE id = ? and create_by = ?";
        jdbcTemplate.update(sql, id, authentication.getName());
    }


}
