package ru.telerman.daos;


import ru.telerman.model.Note;

import java.util.List;


public interface NotesStorageDao {

    void add(Note note);

    List<Note> getList();

    List<Note> getList(String searchString);

    Note get(long id);

    void update(Note note);

    void delete(long id);

}
