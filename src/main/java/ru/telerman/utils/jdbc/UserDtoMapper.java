package ru.telerman.utils.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.telerman.model.UserDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDtoMapper implements RowMapper<UserDto> {

    @Nullable
    @Override
    public UserDto mapRow(ResultSet resultSet, int i) throws SQLException {
        UserDto userDto = new UserDto();
        userDto.setLogin(resultSet.getString("username"));
        userDto.setPassword(resultSet.getString("password"));
        userDto.setMatchingPassword(resultSet.getString("password"));
        return userDto;
    }
}
