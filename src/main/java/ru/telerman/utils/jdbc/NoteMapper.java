package ru.telerman.utils.jdbc;

import org.springframework.jdbc.core.RowMapper;
import ru.telerman.model.Note;

import java.sql.ResultSet;
import java.sql.SQLException;


public class NoteMapper implements RowMapper<Note> {
    @Override
    public Note mapRow(ResultSet rs, int row) throws SQLException {
        Note note = new Note();
        note.setId(rs.getLong("id"));
        note.setHeader(rs.getString("header"));
        note.setBody(rs.getString("body"));
        note.setIcon(rs.getString("icon"));
        note.setCreateBy(rs.getString("create_by"));
        note.setCreateTs(rs.getTimestamp("create_ts"));
        return note;
    }
}
