package ru.telerman.security;

import org.springframework.security.core.userdetails.User;

public interface UserDao {
    User findUser(String login);
}
