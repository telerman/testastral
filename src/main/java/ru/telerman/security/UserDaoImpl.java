package ru.telerman.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import ru.telerman.model.UserDto;
import ru.telerman.utils.jdbc.UserDtoMapper;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate template;

    @Override
    public User findUser(String login) {
        final String sql = "SELECT * FROM users WHERE username = ?";
        List<UserDto> users = template.query(sql, new UserDtoMapper(), login);
        if (users.size() > 1)
            throw new UsernameNotFoundException("Not one user found for current login");
        else if (users.size() == 0)
            return null;
        else {
            UserDto userDto = users.get(0);
            return new User(userDto.getLogin(), userDto.getPassword(), new ArrayList<>());
        }

    }
}
