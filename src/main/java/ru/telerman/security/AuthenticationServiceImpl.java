package ru.telerman.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.telerman.model.UserDto;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;


    @Override
    public String registerUser(UserDto user) {
        if (user.getLogin() == null || user.getPassword() == null && user.getMatchingPassword() == null ||
                user.getLogin().isEmpty() || user.getPassword().isEmpty() || user.getMatchingPassword().isEmpty())
            return "Fill all fields";
        if (!user.getPassword().equals(user.getMatchingPassword()))
            return "Passwords not equals";

        String sql = "SELECT count(*) FROM users WHERE username = ?";
        Integer count = jdbcTemplate.query(sql, (resultSet, i) -> resultSet.getInt(1), user.getLogin()).get(0);
        if (count > 0)
            return "User already exists";
        else {
            sql = "INSERT INTO users(username, password) VALUES (?,?)";
            jdbcTemplate.update(sql, user.getLogin(), passwordEncoder.encode(user.getPassword()));
            sql = "INSERT INTO user_roles(username, role) VALUES (?,?)";
            jdbcTemplate.update(sql, user.getLogin(), "ROLE_USER");
            return null;
        }
    }

}
