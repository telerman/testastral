package ru.telerman.security;

import ru.telerman.model.UserDto;

public interface AuthenticationService {
    String registerUser(UserDto user);
}
