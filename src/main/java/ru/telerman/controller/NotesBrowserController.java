package ru.telerman.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.telerman.daos.NotesStorageDao;
import ru.telerman.model.Note;

import javax.inject.Inject;
import java.util.List;

@Controller
public class NotesBrowserController {

    @Inject
    private NotesStorageDao notesStorageDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView showList() {
        return new ModelAndView("notesBrowser", "notesList", notesStorageDao.getList());
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView showList(@RequestParam("searchString") String searchString){
        List<Note> list = notesStorageDao.getList(searchString);
        ModelAndView mav = new ModelAndView("notesBrowser");
        mav.addObject("notesList", list);
        mav.addObject("searchString", searchString);
        return mav;
    }

    @RequestMapping(value = "/editNote/{id}", method = RequestMethod.GET)
    public ModelAndView editNote(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("noteEditor");

        Note note = notesStorageDao.get(id);
        modelAndView.addObject("note", note);

        return modelAndView;
    }

}
