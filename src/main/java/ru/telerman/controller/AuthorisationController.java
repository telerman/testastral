package ru.telerman.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.telerman.daos.NotesStorageDao;
import ru.telerman.model.UserDto;
import ru.telerman.security.AuthenticationService;
import ru.telerman.security.AuthenticationServiceImpl;

import javax.inject.Inject;

@Controller
public class AuthorisationController {
    @Autowired
    private AuthenticationService authenticationService;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView showRegistrationForm() {
        return new ModelAndView("registrationForm", "user", new UserDto());
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registerUser(@ModelAttribute("user") UserDto user){
        String s = authenticationService.registerUser(user);
        if(s == null)
            return new ModelAndView("redirect:/login");
        else return new ModelAndView("errorPage", "errorString", s);
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(ModelMap model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication instanceof AnonymousAuthenticationToken)
            return "loginForm";
        else return "redirect:/";
    }
}
