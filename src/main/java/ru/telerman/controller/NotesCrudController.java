package ru.telerman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.telerman.daos.IconDao;
import ru.telerman.daos.NotesStorageDao;
import ru.telerman.model.Note;

import javax.inject.Inject;
import java.util.Base64;


@Controller
public class NotesCrudController {

    @Inject
    private NotesStorageDao notesStorageDao;

    @RequestMapping(value = "/createNote", method = RequestMethod.GET)
    public ModelAndView createNote(@ModelAttribute("note") Note note){
        Note newNote = new Note();
        String icon = IconDao.getIcon();
        byte[] encode = Base64.getEncoder().encode(icon.getBytes());
        newNote.setIcon(new String(encode));
        return this.editNote(newNote);
    }

    @RequestMapping(value = "/updateNote", method = RequestMethod.POST)
    public ModelAndView updateNote(@ModelAttribute("note") Note note){
        notesStorageDao.update(note);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/deleteNote/{id}", method = RequestMethod.GET)
    public ModelAndView deleteNote(@PathVariable("id") Long id){
        notesStorageDao.delete(id);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/editNote", method = RequestMethod.POST)
    public ModelAndView editNote(@ModelAttribute("note") Note note) {
        return new ModelAndView("noteEditor", "note", note);
    }
}
